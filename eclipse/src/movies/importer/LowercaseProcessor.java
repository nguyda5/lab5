package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(int i = 0; i < input.size(); i++) {
			String vText = input.get(i);
			vText = vText.toLowerCase();
			asLower.add(vText);
		}
		
		return asLower;
	}
}
