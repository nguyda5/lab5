package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args) throws IOException{
		String source = "C:\\Users\\David\\courses\\java310\\lab5\\test";
		String destination = "C:\\Users\\David\\courses\\java310\\lab5\\test\\completed_test";
		LowercaseProcessor test = new LowercaseProcessor(source, destination, true);
		test.execute();
		
		String source2 = "C:\\Users\\David\\courses\\java310\\lab5\\test\\completed_test";
		String destination2 = "C:\\Users\\David\\courses\\java310\\lab5\\test\\completed_test\\completed_test2";
		RemoveDuplicates test2 = new RemoveDuplicates(source2, destination2, false);
		test2.execute();
	}

}
