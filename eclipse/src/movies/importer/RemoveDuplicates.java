package movies.importer;
import java.util.*;

public class RemoveDuplicates extends Processor{
	public RemoveDuplicates(String sourceDir, String outputDir, boolean srcContainsHeader) {
		super(sourceDir, outputDir, srcContainsHeader);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicates = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			if(!noDuplicates.contains(input.get(i))) {
				noDuplicates.add(input.get(i));
			}
		}
		
		return noDuplicates;
		
	}
}
